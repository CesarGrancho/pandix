<?php

//SESSION
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

error_reporting(E_ALL & ~E_NOTICE ^ E_DEPRECATED);

# Define PATH
define('ROOT', dirname(__FILE__));

#INCLUDE MAIN CLASSES 
require_once ROOT.'/assets/backend/class/main/app.class.php';
require_once ROOT.'/assets/backend/class/main/auxFunctions.class.php';

$settings=[
    "system" => [
        "memory" => "16MB",
        "timeout" => "300", 
        "type" => "HTTP", #SOCKET
        "timezone" => "Europe/Lisbon",
        "session" => [
          "name"=> "PANDIX",
          "expiration"=> "3600" 
        ],
        "headers"=> [
          "Content-Type"=> "text/html; charset=UTF-8",
          "Access-Control-Allow-Origin"=> "*" 
        ]
    ],
    "app" => [		
        "name"=> "PANDIX",
		"BASE" => [
			"URL" => (isset($_SERVER['HTTPS']) ? 'https' : 'http') . "://$_SERVER[HTTP_HOST]/",
			"MAIL" => "no-replay@localhost",
		],
        "DB"=> [
            "obdc"=> "PDO", 
            "type"=> "mysql", 
            "name"=> "project_pandix",
            "user"=> "cesar",
            "pass"=> "12345",
            "url"=> "localhost",
            "prefix" => ""
          
        ],
        "PATHING"=> [			
            "BACKEND" => [
				"TOOLS"=> "./assets/backend/class/extends/_tools",
				"EXTENDS"=> "./assets/backend/class/extends/",
			],            
			"FRONTEND" => [
				"DIR" => "./assets/frontend/views/",
				"MEDIA" => "./assets/frontend/media",
				"UPLOAD_DIR" => "./assets/frontend/media/upload/",
				"DEFAULTS" => [
					"MODE" => "site",
					"SECTION" => "home",
					"ACTION" => "index"	
				]
			]
        ]        
    ],
    "license" => [
        "to" => "",
        "type" => "MIT"
    ]    
];

#echo json_encode($settings);die();


#INIT MAIN : begin 

    //INIT APP OBJECT
    $pandixInitData= [
               "aux" => new auxFunctions(),
               "root" => ROOT,
               "session" => $_SESSION
               ];        
	
	
    $pandix=new app($pandixInitData);    
    
    #try init the application
    if (!$pandix->init($settings)) {
        echo $pandix->__errorLog;
        die();
    }    
    
    //USERS INIT : begin
		$usersInitData= [
						   "db" => $pandix->_DBCON,
						   "aux" => new auxFunctions(),
						   "idUser" => ($pandix->_SESSION['_idUser']>0?$pandix->_SESSION['_idUser']:0), #ADD ID USER GLOBAL
						   "userRoute" => $_REQUEST['c']
						];
		
		$pandix->_USERS=new users($usersInitData);
		
		#EMERGENCY LOGOUT - killswitch
		#session_destroy();print_r($_SESSION);die();		
		

	//USERS INIT : end
    
    //start app operations
    $pandix->routing($pandix->_USERS->_userRoute);    
    
    $pandix->_DBCON=null; #close connection
	
#INIT MAIN : end   

?>